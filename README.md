
AfterOneDotSixOneEight

This visualization software calculates a position of the Fibonacci sequence, and its surrounding neighbors. The division of two neighbors creates a number known as Golden Ratio. The y axis represents this calculated numbers of the Golden Ratio according to the Fibonacci sequence, while its decimals behind the comma are shown along the x-axis.


SOFTWARE

To realize the approach of visualizing the Fibonacci sequence in its fast growing size on one hand, while calculating the continuous shrinking fractional part described by decimals of the Golden Ration on the other hand, an OpenGL application written in C++ using openFrameworks is one suitable way of accomplishing the task. On the very heart of the program, the Fibonacci sequence is created by a for loop, which adds each new calculated Fibonacci number to the end of a list, also known as dynamic array or vector. This “list” will go through another loop, that takes the predecessor, divides it to get the Golden Ration, especially the decimals of its fractional part. The result gets added to yet another “list” (array), thus building the basis of the visualization.

Each calculated Golden Ratio ends up with a different fractional part, depending on which position of the Fibonacci sequence is used to calculate it. The decimals are then used to dedicate the color and height of cubes used for each decimal in the final visuals.


Decimal to Colors:

0→White : 1→Red : 2→Sienna : 3→Orange : 4→Yellow : 5→Green : 6→Blue : 7→Lilac : 8→Pink : 9→ Black


GUI OPTIONS:

FULLSCREEN: Toggles window size to full screen mode

FIBONACCI: Enter a position of the Fibonacci sequence

FRACTION: Enter a position of a decimal in the fractional part of the Golden Ratio

GENERATE: Press button to start calculating the matrix and to generate the visuals

SNAPSHOT: Press button to save an image of the result

TOGGLE Z-VALUE: Add third dimension to the visuals

ORTHO/PERSP: Switches between orthographic and perspective view

HELP: Displays help section

COLLAPSE CONTROLS: Hides/Unhides control panel
